package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"strings"
)

type User struct {
	Username    string `json:"username"`
	apiToken    string
	accessToken string
}

func NewUser(access_token string) *User {
	return &User{
		accessToken: access_token,
	}
}

func (u *User) GetUser() error {
	req, err := http.NewRequest("GET", "https://gitlab.com/api/v4/user", nil)
	if err != nil {
		return err
	}

	values := req.URL.Query()
	values.Add("access_token", u.accessToken)
	req.URL.RawQuery = values.Encode()
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	u.apiToken = RandomString()

	dec := json.NewDecoder(resp.Body)
	var tmp User
	err = dec.Decode(&tmp)
	if err != nil {
		return err
	}

	u.Username = tmp.Username

	if u.Username == "" {
		return errors.New("Got empty user name. Contact admin for any deplyment errors.")
	}

	fmt.Println("Successfully got the username : ", tmp.Username)

	return nil
}

func RandomString() string {
	charset := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	b := make([]byte, 32)
	for i := range b {
		b[i] = charset[rand.Intn(len(charset))]
	}
	return string(b)
}

type Users []*User

func (us Users) WriteTo(w io.Writer) error {
	for _, user := range us {
		_, err := fmt.Fprintf(w, "%s\t%s\t%s\n", user.Username, user.accessToken, user.apiToken)
		if err != nil {
			return err
		}
	}
	return nil
}

func (us *Users) ReadFrom(r io.Reader) error {
	br := bufio.NewReader(r)
	for {
		user := &User{}
		line, err := br.ReadString('\n')
		if err != nil {
			if err != io.EOF {
				return err
			}
			break
		}

		line = strings.TrimSpace(line)

		fmt.Sscanf(line, "%s\t%s\t%s", &user.Username, &user.accessToken, &user.apiToken)
		*us = append(*us, user)
	}
	return nil
}

func (us Users) GetUserForToken(apiToken string) *User {
	for _, user := range us {
		if user.apiToken == apiToken {
			return user
		}
	}
	return nil
}

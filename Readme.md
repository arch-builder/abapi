# abapi

abapi is a server which authenticates the users and acts as a http API outlet for
the [`binner`](https://gitlab.com/arch-builder/binner) service

abapi uses gitlab OAUTH2 flow to authenticate users
and provides a API key to send commands to the binner service.

## Usage

```
aurbinapi -id <client_id> -secret <client_secret> -repo <location of the binary package directory>
```

* *API reference will be added soon.*
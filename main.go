package main

import (
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"
)

type Message struct {
	ReplyType   int64       `json:"reply_type"`
	ReplyString string      `json:"reply_string"`
	Reply       interface{} `json:"reply"`
}

const (
	REPLY_FAILURE  int64 = -1
	REPLY_SUCCESS        = 0
	REPLY_REDIRECT       = 1
	REPLY_APITOKEN       = 2
	REPLY_STATUS         = 3
	REPLY_LIST           = 4
	REPLY_LOG            = 5
	REPLY_WAIT           = 6
	REPLY_USER           = 7
)

type AccessTokenCB struct {
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	Code         string `json:"code"`
	RedirectURI  string `json:"redirect_uri"`
	State        string `json:"state"`
}

type AccessTokenReply struct {
	AccessToken string `json:"access_token"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
}

var clientId string
var clientSecret string

func main() {
	rand.Seed(time.Now().Unix())

	var userDb, repo string
	flag.StringVar(&clientId, "id", "", "Gitlab API Client ID")
	flag.StringVar(&clientSecret, "secret", "", "Gitlab API Client Secret")
	flag.StringVar(&userDb, "db", "users.db", "User database file")
	flag.StringVar(&repo, "repo", "", "Location of the arch binary repo")
	flag.Parse()

	if clientId == "" || clientSecret == "" {
		fmt.Println("Either Client ID or Secret is empty.")
		return
	}

	app := App{states: make(map[string]string), secrets: make(map[string]string), users: new(Users), dbFile: userDb}
	rd, err := os.Open(app.dbFile)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = app.users.ReadFrom(rd)
	if err != nil {
		fmt.Println(err)
		return
	}

	http.HandleFunc("/login", app.loginStart)
	http.HandleFunc("/authCallback", app.authCallback)
	http.HandleFunc("/getToken", app.appToken)
	http.HandleFunc("/done", app.done)
	http.HandleFunc("/build", app.build)
	http.HandleFunc("/status", app.status)
	http.HandleFunc("/list", app.list)
	http.HandleFunc("/log", app.log)
	http.HandleFunc("/success", app.success)
	http.HandleFunc("/user", app.user)

	if repo != "" {
		fmt.Println("Arch Binary Repo running")
		http.Handle("/", http.FileServer(http.Dir(repo)))
	}
	fmt.Println(http.ListenAndServe(":8080", nil))
}

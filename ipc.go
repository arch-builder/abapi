package main

import (
	"encoding/json"
	"errors"
	"net"
)

type IPC struct {
	channel net.Conn
}

func NewIPC(socketFile string) (*IPC, error) {
	var err error
	var conn net.Conn

	conn, err = net.Dial("unix", socketFile)
	if err != nil {
		return nil, err
	}

	return &IPC{channel: conn}, nil
}

func (i *IPC) Close() {
	i.channel.Close()
}

func (i *IPC) Build(user, url string) (*Status, error) {
	enc := json.NewEncoder(i.channel)
	dec := json.NewDecoder(i.channel)

	err := enc.Encode(&Call{
		Method: "build",
		Args:   []string{user, url},
	})
	if err != nil {
		return nil, err
	}

	var status Status
	var reply *Reply = &Reply{}
	reply.Data = &status
	err = dec.Decode(reply)
	if reply.Reply == "error" {
		return nil, errors.New(reply.Data.(string))
	}
	return &status, err
}

func (i *IPC) Status(user, buildId string) (*Status, error) {
	enc := json.NewEncoder(i.channel)
	dec := json.NewDecoder(i.channel)

	err := enc.Encode(&Call{
		Method: "status",
		Args:   []string{user, buildId},
	})
	if err != nil {
		return nil, err
	}

	var status Status
	var reply *Reply = &Reply{}
	reply.Data = &status
	err = dec.Decode(reply)
	if reply.Reply == "error" {
		return nil, errors.New(reply.Data.(string))
	}
	return &status, err
}

func (i *IPC) ListBuilds(user string) (*List, error) {
	enc := json.NewEncoder(i.channel)
	dec := json.NewDecoder(i.channel)

	err := enc.Encode(&Call{
		Method: "list_builds",
		Args:   []string{user},
	})
	if err != nil {
		return nil, err
	}

	var list List
	var reply *Reply = &Reply{}
	reply.Data = &list
	err = dec.Decode(reply)
	if reply.Reply == "error" {
		return nil, errors.New(reply.Data.(string))
	}
	return &list, err
}

type List struct {
	User    string   `json:"user,omitempty"`
	List    []string `json:"list"`
	Message string   `json:"message"`
	IsError bool     `json:"is_error"`
}

type Reply struct {
	Method string      `json:"method"`
	Reply  string      `json:"reply"`
	Data   interface{} `json:"data"`
}

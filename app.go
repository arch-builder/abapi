package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
)

type App struct {
	users   *Users
	states  map[string]string
	secrets map[string]string
	dbFile  string
}

type RedirectStruct struct {
	URL    string `json:"url"`
	Secret string `json:"secret"`
}

var (
	errorBuildIDReq      = "only one argument required : (build ID)"
	errorBuilderInactive = "Server error. Builder service is not running. Contact the admin"
)

func (a *App) loginStart(w http.ResponseWriter, r *http.Request) {
	stateString := fmt.Sprintf("state%d", rand.Int63())
	redirectUrl, _ := url.Parse("https://gitlab.com/oauth/authorize")
	values := redirectUrl.Query()
	values.Add("client_id", clientId)
	values.Add("state", stateString)
	values.Add("scope", "read_user")
	values.Add("response_type", "token")
	values.Add("redirect_uri", "http://172.17.30.207:8080/authCallback")
	redirectUrl.RawQuery = values.Encode()
	w.Header().Set("Content-Type", "application/json")
	dec := json.NewEncoder(w)
	secret := fmt.Sprintf("%d", rand.Int63())
	a.states[stateString] = secret
	dec.Encode(&Message{
		ReplyType:   REPLY_REDIRECT,
		ReplyString: "redirect",
		Reply: RedirectStruct{
			URL:    redirectUrl.String(),
			Secret: secret,
		},
	})
}

func (a *App) authCallback(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	w.Write([]byte(`<html><head></head><body><script>var params=window.location.hash.substr(1);window.location="/done?"+params;</script></body></html>`))
}

func (a *App) done(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	dec := json.NewEncoder(w)

	values := r.URL.Query()
	code := values.Get("access_token")
	state := values.Get("state")

	if code == "" || state == "" {
		w.WriteHeader(403)
		dec.Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "either code or state string is not found",
		})
		return
	}

	_, ok := a.states[state]
	if !ok {
		w.WriteHeader(403)
		dec.Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "state string is not valid",
		})
		return
	}

	u := NewUser(code)
	if err := u.GetUser(); err != nil {
		fmt.Println(err)
		w.WriteHeader(502)
		dec.Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "unable to get user details from github",
		})
		return
	}

	*a.users = append(*a.users, u)
	file, err := os.Create(a.dbFile)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(502)
		dec.Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "database error : unable to access the database",
		})
		return
	}
	defer file.Close()

	err = a.users.WriteTo(file)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(502)
		dec.Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "database error : unable to write to the database",
		})
		return
	}

	a.secrets[a.states[state]] = u.apiToken

	http.Redirect(w, r, "/success", 302)
}

func (a *App) success(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("Success!!!"))
}

func (a *App) appToken(w http.ResponseWriter, r *http.Request) {
	dec := json.NewEncoder(w)
	secret := r.URL.Query().Get("secret")
	if secret == "" {
		w.WriteHeader(403)
		dec.Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "empty secret sent",
		})
		return
	}

	for key, val := range a.states {
		if val == secret {
			apiToken, ok := a.secrets[secret]
			if !ok {
				dec.Encode(&Message{
					ReplyType:   REPLY_WAIT,
					ReplyString: "wait",
					Reply:       "Authentication flow is not done yet",
				})
				return
			}

			dec.Encode(&Message{
				ReplyType:   REPLY_APITOKEN,
				ReplyString: "api_token",
				Reply:       apiToken,
			})

			delete(a.states, key)
			delete(a.secrets, secret)
			return
		}
	}

	w.WriteHeader(403)
	dec.Encode(&Message{
		ReplyType:   REPLY_FAILURE,
		ReplyString: "failure",
		Reply:       "secret not associated with any authentication flow",
	})
	return

}

type Args struct {
	ApiToken string   `json:"api_token"`
	Args     []string `json:"args"`
}

type ListReply struct {
	List    []string `json:"list"`
	Message string   `json:"message"`
	IsError bool     `json:"is_error"`
}

type Status struct {
	User      string     `json:"user"`
	BuildID   string     `json:"build_id"`
	URL       *url.URL   `json:"url"`
	Start     int64      `json:"start_timestamp"`
	End       int64      `json:"end_timestamp"`
	Completed bool       `json:"completed"`
	Success   bool       `json:"success"`
	Message   string     `json:"message"`
	Packages  []*PkgInfo `json:"packages,omitempty"`
}

type PkgInfo struct {
	Pkgname   string `json:"pkgname,omitempty"`
	Pkgver    string `json:"pkgver,omitempty"`
	Pkgdesc   string `json:"pkgdesc,omitempty"`
	URL       string `json:"pkgurl,omitempty"`
	BuildDate int64  `json:"pkgdate,omitempty"`
	Packager  string `json:"pkger,omitempty"`
	Size      int64  `json:"size,omitempty"`
	Arch      string `json:"arch,omitempty"`
	License   string `json:"license,omitempty"`
}

type Call struct {
	Method string   `json:"method"`
	Args   []string `json:"args"`
}

func (a *App) status(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	arg := Args{}
	err := json.NewDecoder(r.Body).Decode(&arg)
	if err != nil {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "json data decode error : " + err.Error(),
		})
		return
	}

	user := a.users.GetUserForToken(arg.ApiToken)
	if user == nil {
		w.WriteHeader(407)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "user not found in the database",
		})
		return
	}

	if len(arg.Args) != 1 {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       errorBuildIDReq,
		})
		return
	}

	ipc, err := NewIPC("/tmp/binner.sock")
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       errorBuilderInactive,
		})
		return
	}
	defer ipc.Close()

	status, err := ipc.Status(user.Username, arg.Args[0])
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       err.Error(),
		})
		return
	}

	json.NewEncoder(w).Encode(&Message{
		ReplyType:   REPLY_STATUS,
		ReplyString: "success",
		Reply:       status,
	})
}

func (a *App) build(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	arg := Args{}
	err := json.NewDecoder(r.Body).Decode(&arg)
	if err != nil {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "json data decode error : " + err.Error(),
		})
		return
	}

	user := a.users.GetUserForToken(arg.ApiToken)
	if user == nil {
		w.WriteHeader(407)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "user not found in the database",
		})
		return
	}

	if len(arg.Args) != 1 {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       errorBuildIDReq,
		})
		return
	}

	ipc, err := NewIPC("/tmp/binner.sock")
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       errorBuilderInactive,
		})
		return
	}

	status, err := ipc.Build(user.Username, arg.Args[0])
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       err.Error(),
		})
		return
	}

	json.NewEncoder(w).Encode(&Message{
		ReplyType:   REPLY_STATUS,
		ReplyString: "success",
		Reply:       status,
	})
}

func (a *App) list(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	arg := Args{}
	err := json.NewDecoder(r.Body).Decode(&arg)
	if err != nil {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "json data decode error : " + err.Error(),
		})
		return
	}

	user := a.users.GetUserForToken(arg.ApiToken)
	if user == nil {
		w.WriteHeader(407)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "user not found in the database",
		})
		return
	}

	if len(arg.Args) != 0 {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "no arguments expected.",
		})
		return
	}

	ipc, err := NewIPC("/tmp/binner.sock")
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       errorBuilderInactive,
		})
		return
	}

	list, err := ipc.ListBuilds(user.Username)
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       err.Error(),
		})
		return
	}
	list.User = user.Username

	json.NewEncoder(w).Encode(&Message{
		ReplyType:   REPLY_LIST,
		ReplyString: "success",
		Reply:       list,
	})
}

type LiveLog struct {
	User    string `json:"user"`
	BuildID string `json:"build_id"`
	Stderr  string `json:"stderr"`
	Stdout  string `json:"stdout"`
}

func (a *App) log(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	arg := Args{}
	err := json.NewDecoder(r.Body).Decode(&arg)
	if err != nil {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "json data decode error : " + err.Error(),
		})
		return
	}

	user := a.users.GetUserForToken(arg.ApiToken)
	if user == nil {
		w.WriteHeader(407)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "user not found in the database",
		})
		return
	}

	if len(arg.Args) != 1 {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       errorBuildIDReq,
		})
		return
	}

	logDir := filepath.Join("/var/log/binner/", user.Username, arg.Args[0])

	st, err := os.Stat(logDir)
	if err != nil {
		if os.IsNotExist(err) {
			w.WriteHeader(403)
			json.NewEncoder(w).Encode(&Message{
				ReplyType:   REPLY_FAILURE,
				ReplyString: "failure",
				Reply:       "Build not found : " + arg.Args[0],
			})
			return
		}

		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "Error while opening the log directory. Contact the admin.",
		})
		return
	}

	if !st.IsDir() {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "Error while opening the log directory. Contact the admin. (Debug : directory expected).",
		})
		return
	}

	stderrFile := filepath.Join(logDir, "stderr")
	stdoutFile := filepath.Join(logDir, "stdout")

	stderrbs, err := ioutil.ReadFile(stderrFile)
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "Error while opening the log file. Contact the admin. (stderr)",
		})
		return
	}

	stdoutbs, err := ioutil.ReadFile(stdoutFile)
	if err != nil {
		w.WriteHeader(502)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "Error while opening the log file. Contact the admin. (stdout)",
		})
		return
	}

	json.NewEncoder(w).Encode(&Message{
		ReplyType:   REPLY_LOG,
		ReplyString: "success",
		Reply: &LiveLog{
			User:    user.Username,
			BuildID: arg.Args[0],
			Stderr:  base64.StdEncoding.EncodeToString(stderrbs),
			Stdout:  base64.StdEncoding.EncodeToString(stdoutbs),
		},
	})
}

func (a *App) user(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	arg := Args{}
	err := json.NewDecoder(r.Body).Decode(&arg)
	if err != nil {
		w.WriteHeader(403)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "json data decode error : " + err.Error(),
		})
		return
	}

	user := a.users.GetUserForToken(arg.ApiToken)
	if user == nil {
		w.WriteHeader(407)
		json.NewEncoder(w).Encode(&Message{
			ReplyType:   REPLY_FAILURE,
			ReplyString: "failure",
			Reply:       "user not found in the database",
		})
		return
	}

	json.NewEncoder(w).Encode(&Message{
		ReplyType:   REPLY_USER,
		ReplyString: "user",
		Reply:       user.Username,
	})
}
